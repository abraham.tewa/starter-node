// ============================================================
// Functions
/**
 * Return hello message
 * @param [name] - Name of the person to say hello to
 * @public
 */
function getHelloMessage(name?: string): string {
  if (name) {
    return `Hello ${name} !`;
  }

  return 'Hello you !';
}

/**
 * Display hello message
 * @param name - Name of the person to say hello to
 * @returns
 */
function sayHello(name?: string): void {
  const message = getHelloMessage(name);
  // eslint-disable-next-line no-console
  console.log(message);
}

// ============================================================
// Exports
export {
  getHelloMessage,
  sayHello,
};
