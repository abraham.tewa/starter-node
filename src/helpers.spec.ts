/* eslint-env node, jest */

// ============================================================
import { faker } from '@faker-js/faker';
// Import packages
import { assert } from 'chai';

// ============================================================
// Import modules
import * as helpers from './helpers';

const consoleLogSpy = jest.spyOn(global.console, 'log').mockImplementation(() => undefined);

beforeEach(() => {
  consoleLogSpy.mockClear();
});

// ============================================================
// Tests
describe('getHelloMessage', () => {
  it('Should return a string', () => {
    const helloMessage = helpers.getHelloMessage();

    assert.isString(helloMessage);

    expect(helloMessage).toMatchSnapshot();
  });

  it('Should include name', () => {
    const name = faker.string.alphanumeric(10);
    const helloMessage = helpers.getHelloMessage(name);

    assert.isString(helloMessage);
    assert.isTrue(helloMessage.includes(name));
  });
});

describe('sayHello', () => {
  it('should log the default message if no name provided', () => {
    consoleLogSpy.mockClear();

    helpers.sayHello();

    expect(consoleLogSpy).toHaveBeenCalledTimes(1);
    expect(consoleLogSpy).toMatchSnapshot();
  });
});
