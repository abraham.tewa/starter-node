import yargs from 'yargs';
import { sayHello } from './helpers';

type Argv = {
  name: string,
};

function cli() {
  return yargs.usage('$0 <cmd> [args]')
    .command<Argv>('hello [name]', 'Display hello message', (command) => {
    command.positional('name', {
      describe: 'The name to say hello to',
      type: 'string',
    });
  }, (argv) => {
    sayHello(argv.name);
  })
    .help()
    .argv;
}

export default cli;
